package com.billingsystem.billingsystem.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bill {
	
	@Id
	private long billId;
	private String billDate;
	private long noOfItems;
	private long totalAmount;
	
	
	public Bill(long billId, String billDate, long noOfItems, long totalAmount) {
		super();
		this.billId = billId;
		this.billDate = billDate;
		this.noOfItems = noOfItems;
		this.totalAmount = totalAmount;
	}
	
	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getBillId() {
		return billId;
	}
	public void setBillId(long billId) {
		this.billId = billId;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public long getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(long noOfItems) {
		this.noOfItems = noOfItems;
	}
	public long getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}
	

}
